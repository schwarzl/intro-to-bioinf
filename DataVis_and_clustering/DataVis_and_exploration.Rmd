---
title: "Data visualisation and exploration"
author: "Florian Huber"
date: "`r Sys.Date()`"
output:
  BiocStyle::html_document:
    toc: yes
    toc_float: yes
  BiocStyle::pdf_document:
    highlight: tango
    toc: yes
editor_options: 
  chunk_output_type: console
---

```{r, message = FALSE}
library(tidyverse)
library(ggbeeswarm)
```


```{r, eval = FALSE, include = FALSE}
data("x")
x
?x
dim(x)
exprs(x)
class(exprs(x))
exprs(x)[1:5, 1:5]

selectedProbes <- c( Fgf4 = "1420085_at", Gata4 = "1418863_at",
                   Gata6 = "1425463_at",  Sox2 = "1416967_at")

genes <- as_tibble(exprs(x), rownames = "probe")

genes <- filter(genes, probe %in% selectedProbes) %>%
  gather(key = sample, value = value, -probe)
genes

genes$gene <- names(selectedProbes)[ match(genes$probe, selectedProbes) ]

##

head(pData(x), n = 2)
class(pData(x))

(metadata <- as_tibble(pData(x), rownames = "rowname"))

groups <- group_by(metadata, sampleGroup) %>%
  dplyr::summarise(n = n(), colour = unique(sampleColour))
groups
```

# Importing data

## General remarks

Data import is a complex topic and is often more challenging than it should be 
because people use different conventions for separating fields, indicating NA 
values etc. R can import data both locally and remotely and can also connect to 
data bases, scrape data from websites, or connect to GoogleDrive. 

Here, we will just briefly cover data import from local files. The base R 
function is `read.table()` and has plenty of options. 
However, we recommend using the readr package because it has more options for 
parsing columns. Check out 
the [tutorial](https://readr.tidyverse.org/) or the [data import cheat sheet](https://github.com/rstudio/cheatsheets/raw/master/data-import.pdf) 
to learn more about its functionality. 

If you have to import data from Excel sheets, check out the package 
[readxl](https://readxl.tidyverse.org/). 

## Importing the `cells` data set

In this session we will talk about data visualisation and exploration using 
the [ggplot2 package](https://ggplot2.tidyverse.org/). 

To illustrate the different needs that commonly arise during data analysis we 
will work with the "cells" data set. This data set is the result of an imaging 
experiment where cells were plated into two wells and were either treated or 
untreated. Imaging was performed in two channels (mCherry and GFP) and at 
different exposure times. After segmentation, light intensities were recorded 
for each individual cell. 

First, let's import the data from the `../data` directory. 

```{r}
# brackets around an expression will evaluate it so you don't have to type 
# cells again after assignment to see its contents
(cells <- read_delim("../data/ImagedCells.csv", delim = ";"))
```

### Exercise: data import

Alas, data import in real life is not so easy. Out in the wild something else 
would have happened: your collaborator put a file `ImagedCells_reallife.csv` in the 
directory `../data` and because she is from Germany her version of Excel used 
"," instead of "." as the decimal mark. Moreover, there are two other little 
problems with the file and when you run the above command weird stuff happens. 
Of course nobody bothered to tell you what you have to do and it's already 8 pm 
in the lab so you 
can't ask anyone anymore. You still want to get that data import done! Look at 
the file in a text editor and then read the documentation of `read_delim()` (or 
Google your problems). What related function do you have to call and what 
options do you have to change to import your data without any failures? 


# Inspecting data sets: getting an overview

After importing data, the first thing that we usually want to do is to get an 
overview of the data. This includes simply "looking" at the data, understanding 
which variables there are, what the number ranges are for different variables 
etc. 

The following functions are useful to get a broad overview.

```{r, eval = FALSE}
# not run: run in the console to see the output
str(cells)
glimpse(cells)
head(cells)
summary(cells)
# View(cells)
```

These functions are good to get an idea of the data range involved.

```{r}
range(cells$cell_area)
max(cells$gfp_T0)
which.max(cells$gfp_T0)
quantile(cells$gfp_T0)
quantile(cells$gfp_T0, probs = c(0.01, 0.25, 0.75, 0.99))
unique(cells$Well)
n_distinct(cells$Well)
```

Function `table()` is highly useful for counts and cross-tabulation.

```{r}
table(cells$Well)
table(cells$Well, cells$treated)
```

And of course R has all classical statistical summary functions.

```{r}
mean(cells$roundness)
median(cells$roundness)
cor(cells$cell_area, cells$gfp_T1, method = "spearman")
cor.test(cells$cell_area, cells$gfp_T1, method = "spearman")
```

# Plotting

We will only talk about ggplot2 in this course. Other R graphics systems 
include the base system, grid, and lattice. 

[ggplot2](https://ggplot2.tidyverse.org/) is a package by Hadley Wickham. It 
tries to implement a "grammar of 
graphics" so that there is a consistent translation between the type of plot 
that one wants to create and the commands that one needs to call. While the 
theoretical overhead is a bit higher than in the base system, it is worth the 
effort because at some point one can "speak plot". 

Note that making good plots takes time and it is often not worth tweaking and 
optimising your plot because you do not know which plots you are actually going 
to use for your papers or presentations.

There are always a lot of options of what one might want to change in a plot. 
That is why your best friend will be the 
[ggplot2 documentation](https://ggplot2.tidyverse.org/reference/) and, of 
course, Stack Overflow. 

## Basic functionality: scatter plots

Scatter plots are very useful to understand the basic working principles of 
ggplot2. 

In ggplot2, plots are created by first calling the function 
`ggplot(data, mapping)` and then 
adding different layers to the plot using `+`. These layers can be so-called 
"geoms" such as points, lines, etc. but also statistical transformations, axis 
labels, and other modifications.

A ggplot always starts with a call to `ggplot(data, mapping)`. You need to specify 
which data ggplot should 
operate on and an aesthetic mapping, which indicates which variables should be 
mapped to which property of the plot. 

```{r}
# make sure that the ggplot2 package is actually loaded: library(ggplot) or 
# library(tidyverse)
ggplot(data = cells, aes(x = gfp_T0, y = mCh_T0))
```

At this stage, you will not be able to see anything because you have not added 
any "geom" to the plot:

```{r}
ggplot(data = cells, aes(x = gfp_T0, y = mCh_T0)) + 
  geom_point()
```

ggplot objects can be assigned to variables and evaluated later on, which will 
display the plot:

```{r}
p <- ggplot(data = cells, aes(x = gfp_T0, y = mCh_T0)) + 
  geom_point()

p
```

Add `labs()` to the ggplot command to annotate the axes and add a title.

```{r}
p <- p + labs(x = "GFP at time point T0", y = "mCherry at time point T0", 
  title = "mCherry vs. GFP intensity")

p
```

## Saving ggplots

ggplot2 plots can be saved with function `ggsave(filename, plot)`. If the `plot` 
argument is not specified, the last displayed plot is saved. The filetype is 
inferred from the extension.

```{r}
# units are in inches
ggsave(filename = "A_plot_has_been_born.pdf", plot = p, width = 5, height = 5)
```


## Aesthetic mappings

Aesthetic mappings are used to add additional visual properties to the plot. 
Let us use colour to indicate whether cells have been treated or not:

```{r}
ggplot(data = cells, aes(x = gfp_T0, y = mCh_T0)) + 
  geom_point(aes(colour = treated)) + 
  labs(x = "GFP at time point T0", y = "mCherry at time point T0", 
     title = "mCherry vs. GFP intensity")

# any part of the plotting command can be saved in a variable:
my_label <- labs(x = "GFP at time point T0", y = "mCherry at time point T0", 
  title = "mCherry vs. GFP intensity")

ggplot(data = cells, aes(x = gfp_T0, y = mCh_T0)) + 
  geom_point(aes(colour = treated)) + 
  my_label
```

### Exercise: aesthetics

ggplot2 also understands other aesthetics such as shape, size, and alpha. Map 
other aesthetics to treated (or any other variable you like) and see what 
happens. Can you map more than one aesthetic to the same variable? Next, what 
if you do not want use colour to distinguish groups but rather you want to have 
red points instead of black ones. To do this you need to set `colour = "red"`. 
Careful: there is a difference between putting this into the `aes()` command 
and outside of it (as an additional argument to the geom). How can you change 
the shape that is displayed to e.g. hollow circles? Read the documentation, 
ask the all-knowing internet or talk to your neighbour if you do not know the 
answer. 


## Adding model fits to ggplot2 

Something that one wants to do quite often is to plot a model fit such as a 
linear regression in a plot. 

```{r}
ggplot(data = cells, aes(x = gfp_T0, y = mCh_T0)) + 
  geom_point(aes(colour = treated)) + 
  geom_smooth(method = "lm", se = FALSE) + 
  my_label
```

This was not quite what we wanted. The colour aesthetic is only used for 
`geom_point()`. We either have to specify the colour mapping again for 
`geom_smooth()` or specify it at the "top level", the `ggplot()` command. We 
say that the other layers *inherit* the aesthetic from the `ggplot()` command. 

```{r}
ggplot(data = cells, aes(x = gfp_T0, y = mCh_T0)) + 
  geom_point(aes(colour = treated)) + 
  geom_smooth(aes(colour = treated), method = "lm", se = FALSE) + 
  my_label

# using inheritance

ggplot(data = cells, aes(x = gfp_T0, y = mCh_T0, colour = treated)) + 
  geom_point() + 
  geom_smooth(method = "lm", se = FALSE) + 
  my_label
```

If you want to know more about the model parameters fit the model outside of 
ggplot. 

```{r}
cells %>%
  filter(treated == "yes") %>%
  lm(formula = mCh_T0 ~ gfp_T0)

group_by(cells, treated) %>%
  summarise(my_mod = list(lm(mCh_T0 ~ gfp_T0)))
```


### Exercise: model fits

Create a new variable containing only cells from well "A1" that have an area 
smaller than 600 (bigger cells are "weird"). Then plot that data and run a 
linear regression of GFP vs. mCherry at time point 0. How big is R2? Is the 
correlation significant? Can you extract the residuals? *Do not use Dr. Google* 
but play around with functions `str()`, `View()`, `summary()` or read the help 
file of `?lm()` to solve your problem. Plot the residuals using `plot()`. 

Change the `method` argument to `geom_smooth()` to "loess" and see what 
happens. 


## Displaying quantities and distributions

### Barplots

Each geom is associated with a statistical transformation. In the case of 
scatter plots the "transformation" consists of returning the values unchanged, 
which also called 
"identity transformation". However, other plotting types make extensive use of 
statistical transformations before displaying the data. `geom_bar()`, for 
example, will by default plot the counts of the observations in the different 
groups. If you want to display the values themselves you need to specify the 
"stat" argument and set it to "identity". 

```{r}
ggplot(cells, aes(x = treated)) + 
  geom_bar()

cells[1:6, ] %>%
  ggplot(aes(x = cellID, y = gfp_T0)) + 
  geom_bar(stat = "identity")
```

### Boxplots

```{r}
ggplot(cells, aes(x = Well, y = gfp_T0)) + 
  geom_boxplot()
```

### Violin plots and beeswarm plots, jittering

```{r}
ggplot(cells, aes(x = Well, y = gfp_T0)) + 
  geom_violin()

ggplot(cells, aes(x = Well, y = gfp_T0)) + 
  geom_violin() + 
  geom_point(alpha = 0.5)
```

The problem here is that the points are stacked on top of each other. To 
prevent this, you can add some noise to the points, which is also known as 
"jittering". Be careful not to change the actual values if you use this. 

```{r}
# set height to 0!
ggplot(cells, aes(x = Well, y = gfp_T0)) + 
  geom_violin() + 
  geom_point(alpha = 0.5, position = position_jitter(width = 0.1, height = 0))
```

Beeswarm plots are an excellent alternative to jittering, especially for low- 
or medium-sized data sets. 

```{r}
ggplot(cells, aes(x = Well, y = gfp_T0)) + 
  geom_violin() + 
  ggbeeswarm::geom_beeswarm(cex = 2)
```

### Distributions and density plots

```{r}
ggplot(cells, aes(x = gfp_T0)) + 
  geom_histogram(bins = 20)
```

By default, histograms will plot counts. If you want to plot the densities you 
have to specify `y = ..density..`. The `..` syntax means that you want to 
access a variable that was computed internally by ggplot2 when running 
`geom_histogram()`. 

```{r}
ggplot(cells, aes(x = gfp_T0)) + 
  geom_histogram(aes(y = ..density..), bins = 20)
```

To plot a smoothed density curve, add `geom_density()`. 

```{r}
ggplot(cells, aes(x = gfp_T0)) + 
  geom_histogram(aes(y = ..density..), bins = 20) + 
  geom_density()

ggplot(cells, aes(x = gfp_T0)) + 
  geom_histogram(aes(y = ..density..), bins = 20) + 
  geom_density(adjust = 4)
```

A nice alternative to histograms is the *frequency polygon*.

```{r}
ggplot(cells, aes(x = gfp_T0)) + 
  geom_freqpoly(bins = 20)
```

### Empirical cumulative distribution

```{r}
ggplot(cells, aes(x = gfp_T0, colour = Well)) + 
  stat_ecdf()
```


### Exercise: displaying error bars

In many applications you want to display some kind of summary statistic 
together with an error bar. There is a dedicated geom for displaying errors: 
`geom_errorbar()`. Alternatives for displaying measurement ranges are 
`geom_crossbar()`, `geom_pointrange`, or `geom_ribbon()`. Use your knowledge 
of `group_by()`, `filter()`, `summarise()` etc. and the the ggplot2 
documentation to do the following _for each well_: 

* First, exclude the 1% brightest cells in the `gfp_T0` channel as these are just some bright dust particles.
* Then add a new column ratio that contains the ratios of mCh_T0 divided by gfp_T0. 
* Summarise the data set such that you get a column of the mean of `ratio` = `mean_ratio`, a column for its standard deviation (s.d.), one for `mean_ratio` minus the s.d. and one for `mean_ratio` plus the s.d.

Then use your output to plot a bar plot displaying the two wells together with 
their error bars. Try also the other geoms such as `geom_pointrange()`. 
Experiment also with the different options of displaying the data points using 
the geoms from above (or whatever else you find on the ggplot2 website). 


### Exercise: plotting distributions on top of each other

Using the cells data, display the distributions of the variable of your choice 
using a histogram. Distinguish the variable `treated` by mapping to `colour` or 
`fill`. Are the histograms stacked on top of each other or does one histogram 
overlap the other? How could you find out? Experiment by setting the argument 
`position` to "identity" or "fill". What happens? 



## Dealing with overplotting

Different options exist, depending on the amount of overplotting. These range 
from changing the transparency or shape of the plots to displaying binned 
counts or densities. 

```{r}
p <- ggplot(data = cells, aes(x = gfp_T0, y = mCh_T0))

p + geom_point(alpha = 0.2)

p + geom_point(shape = 1)

p + geom_point(aes(colour = treated)) + 
  geom_density_2d()

p + geom_point(aes(colour = treated)) + 
  geom_density_2d(h = 20)

p + stat_density_2d(geom = "polygon", aes(fill = ..level..))

p + stat_density_2d(geom = "polygon", aes(fill = ..level..))

p + geom_hex()

p + geom_hex(binwidth = c(20, 20))
```


## The time series challenge, aka tidy data aka my data is in a bad shape

When we look at our `cells` data, we can see that it is in a useful format to 
plot correlations between channels and time points. However, what if we wanted 
to plot different colour channels (i.e. gfp vs. mCherry), not wells, in 
different colours? In principle, all 
we need to do is to map the colour aesthetic to a channel variable. However, 
while the information on colour channels is in the table, it is not in a format 
that can be used for the purpose since the variable we want to map is "spread 
out" across several columns.

To solve this problem, we need to "reshape" our table. Instead of having 
columns such as `gfp_T0`, which actually represent several pieces of 
information, we want to have 3 new columns, one for the colour channel, one 
for the time point, and one for the intensity. 

Data where each column represents a variable and each row an observation is 
also called *tidy data*. It is usually preferable to have your data in a tidy 
format. The reason is that there is usually just one way of having tidy data. 
This means that tidy data is a sort of standard that makes it easier to think 
about your data. The idea of tidy data is very powerful and inspired the name 
"tidyverse". Check the free online book [R for Data Science](https://r4ds.had.co.nz/) by Hadley 
Wickham and Garrett Grolemund for a more thorough introduction into the topic. 

![Following three rules makes a dataset tidy: variables are in columns, observations are in rows, values are in cells. Taken from: Hadley Wickham and Garrett Grolemund: R for data science](tidy_data.png)

So how can we make data tidy? The most important function to tidy up your data 
is the `gather()` function. 
It gathers your untidy data columns into a new column specified by the key 
argument. The values end up in the values argument. 

![The "gather" operation](./gather.png){width=150%}

```{r}
# before: 
cells

# and after:
cells_tidy <- gather(cells, gfp_T0, mCh_T0, gfp_T1, gfp_T2, gfp_T3, mCh_T1, 
  mCh_T2, mCh_T3, key = channel_tpt, value = int)

cells_tidy
```

However, our data is not yet completely tidied up. We still need to separate the 
`channel_timepoint` column into two new columns. For this, there is the 
`separate()` function in the dplyr package. 

```{r}
cells_tidy <- separate(cells_tidy, col = channel_tpt, 
  into = c("channel", "tpt"), sep = "_")
cells_tidy
```

Now we can plot our time series: the x-axis needs to be mapped to the time 
point, the y-axis to the light intensity. We only visualise "gfp" here. 

```{r}
cells_tidy <- filter(cells_tidy, channel == "gfp")

p <- ggplot(cells_tidy, aes(x = tpt, y = int, colour = treated))
```

The geom we need is `geom_line()`:

```{r}
p + geom_line()
```

What happened? We did not tell ggplot which data points it should connect. To 
do this, we need the `group` aesthetic. In the following command we tell 
geom_line that it should connect data points if they share a cellID.

```{r}
p + geom_line(aes(group = cellID))
```


### Exercise: tidying up data

The data for this exercise has been adapted from [materials by Bernd Klaus](https://www.huber.embl.de/users/klaus/stat_methods_bioinf/graphics_bioinf.html). 

For this exercise we will load a dataset containing single-cell transcriptomics 
data. It is in the form of a count matrix in which each row represents a gene 
and each column represents one cell. 

```{r}
(load("../data/mtec_counts.RData"))
mtec_counts
```

Now create a subtable containing only the first three rows and the first 9 
cells. Then tidy the data = turn them into a "long format" so that one column 
contains the gene name, the second the cell identifier ("cell2", "cell17" etc.), 
and the third column should contain the counts. 

Bonus exercise: use the tidied up table to creat a barplot that shows on the 
x-axis the ensembl_ID and on the y-axis the counts. Display the information of 
how many counts belong to which cell by mapping the "fill" aesthetic to the 
cell variable. Set the position argument to "dodge": what happens? 



## Faceting plots

Sometimes we want to distinguish groups not by colour or another aesthetic but 
rather show them in separate panels. This is really easy with ggplot: you 
simply need to add `facet_wrap(rowvar ~ colvar)` or 
`facet_grid(rowvar ~ colvar)` to the ggplot2 object. So in the above example 
we could have written:

```{r}
p + geom_line(aes(group = cellID)) + 
  facet_wrap( ~ treated) # facet_grid also works
```



## Tweaking plots

While ggplot2's defaults are usually quite pleasing you may occasionally want 
to change things such as the range of data that is displayed, the colour 
palette, the order of columns etc. What follows are some common things that 
one may want to change. 


### Changing colours: `scale_colour_*`

Something you want to change quite often is the colours that are used. For 
example, you may want to keep a common colour scheme throughout your 
presentation/paper or you may want to use a colorblind-friendly scheme. 

To specify colours yourself you can change the "colour scales". If you mapped 
the colour aesthetic use `scale_colour_manual()`, if you mapped the fill 
aesthetic use `scale_fill_manual()` etc. With these functions you can also 
change the legend title and the legend labels. 

```{r}
p <- ggplot(data = cells, aes(x = gfp_T0, y = mCh_T0)) + 
  geom_point(aes(colour = treated))

p + scale_colour_manual("Treated?", labels = c("No", "Yes"), 
  values = c("black", "red"))
```

You can also use RGB and Hex codes. Check out the [colorbrewer website](www.colorbrewer2.org). 
It has great suggestions for pleasing colour schemes. 

```{r}
# ggplot also accepts RGB and Hex codes:
p + scale_colour_manual("Treated?", labels = c("No", "Yes"), 
  values = c("#1b9e77", "#d95f02"))
```


### Changing axis limits and scales. 

How can we change the range of data that is shown? 

There are two ways to do this: 

  1. You can change the range that is _displayed_, i.e. you effectively zoom in to the plot. This is done using `coord_cartesian(xlim = , ylim = )`. 
  2. You can exclude certain data from the data set while plotting. This is done using `scale_x_continuous(limits = )`. 

The difference between 1 and 2 is that in 2 data points are really excluded 
from the data before plotting. This means that summary statistics or model fits 
will be affected. Compare the following plots:

```{r}
p 

p + coord_cartesian(xlim = c(250, 300), ylim = c(200, 250)) + 
  geom_smooth(method = "lm", aes(colour = treated))

p + scale_x_continuous(limits = c(250, 300)) + 
  scale_y_continuous(limits = c(200, 250)) + 
  geom_smooth(method = "lm", aes(colour = treated))
```


### Changing the order of items: factors

Let's think back to our barplots.

```{r}
gfp_summary <- 
  group_by(cells, Well) %>% 
  summarise(mean_gfp = mean(gfp_T0), 
    lower = mean_gfp - sd(gfp_T0), 
    upper = mean_gfp + sd(gfp_T0))

ggplot(gfp_summary, aes(x = Well, y = mean_gfp)) + 
  geom_bar(stat = "identity")
```

Why does ggplot put the bar "A1" first and then "B1"? So the items are sorted 
alphabetically. How can we change that? 

To do this, we must first understand factors. Internally, the variable Well is 
converted to a *factor*. A factor is a data structure that is printed like a 
character vector at the console but under the hood, it is an integer vector in 
combination with a lookup table: the integer values are decoded using the 
so-called *levels* of the factor. By default, the levels are sorted 
alphabetically. So the solution is to construct the factor ourselves with the 
levels in the desired order.

```{r}
(f <- factor(gfp_summary$Well))
as.integer(f)
levels(f)

(f2 <- factor(gfp_summary$Well, levels = c("B1", "A1")))
as.integer(f2)
levels(f2)

gfp_summary$Well <- factor(gfp_summary$Well, levels = c("B1", "A1"))

ggplot(gfp_summary, aes(x = Well, y = mean_gfp)) + 
  geom_bar(stat = "identity")
```

