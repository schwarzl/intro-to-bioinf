# Introduction to Bioinformatics with R and Bioconductor

## Schedule

### Day 1, Monday, April 1st
[0] Introduction (Thomas Schwarzl)

[1] 14:30 - 15:30 R refresher (Thea Van Rossum)

https://www.huber.embl.de/users/klaus/tidyverse_R_intro/R-lab.html
Covering: sections 1-5, 6.1, 6.4

[2] 16:00 - 18:00 Data preprocessing  (Thea Van Rossum)

https://www.huber.embl.de/users/klaus/graphics_and_data_handling/graphics_and_data_handling.html
Covering: section 10

18:00 - 19:00 Dinner (Canteen)

### Day 2, Tuesday, April 2nd

[3] 09:05 - 11:00 Graphics and Data Handling (Thomas Schwarzl)

https://www.huber.embl.de/users/klaus/graphics_and_data_handling/graphics_and_data_handling.html

[4] 11:15 - 12:30 Data Visualization and Exploration (Florian Huber)

[4] 13:30 - 15:30 Data Visualization and Exploration (Florian Huber)

https://www.huber.embl.de/users/klaus/stat_methods_bioinf/graphics_bioinf.html

[5] 16:00 - 18:00 Clustering and Dimension reduction (Florian Huber)

18:00 - 19:00 Dinner (Canteen)

### Day 3: Wednesday, April 3rd

[6] 09.05 - 11:00 Hypothesis Testing (Florian Huber)

https://www.huber.embl.de/users/klaus/teaching.html#hypothesis_testing_%E2%80%93_concise

[6] 11:30 - 12:30 Hypothesis Testing (Florian Huber)

[7] 13:30 - 15:30 Introduction to Bioconductor (Thomas Schwarzl)

https://github.com/Bioconductor/CSAMA/blob/2018/lecture/1-monday/lecture-01-bioc-intro.Rmd

https://github.com/Bioconductor/CSAMA/tree/2018/lab/1-monday/lab-01-intro-to-r-bioc

[8] 16:00 - 17:45 Statistical testing for high-throughput data (Thomas Schwarzl)

https://git.embl.de/klaus/stat_methods_bioinf

18:00 - Shuttle downtown

19:00 - Dinner

### Day 4: Thursday, April 4rd

[9] 09:05 - 11:00 Analysis of high-throughput count data (Thomas Schwarzl)

http://www-huber.embl.de/users/msmith/.msmb/tufte/ (Pw: mls)

[10] 11:30 - 12:30 Data annotation (Thomas Schwarzl)

[11] 13:30 - 15:30 Q + A (Thomas Schwarzl)

